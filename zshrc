#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

autoload -Uz promptinit
promptinit
prompt pure

PATH=$PATH:~/.local/bin
PATH=$PATH:~/.cabal/bin

alias twelf="rlwrap twelf-server"
alias th="cd ~/academic/thesis/report"
alias vi="emacsclient"

export CS242SVN="https://subversion.ews.illinois.edu/svn/sp18-cs242/tosun2"

function pbcopy () {
  cat $1 | xclip -i -selection clipboard
}

function fixspotify () {
  wmctrl -r spotify -b 'toggle,fullscreen'
}

function bright () {
  R=$(python3 -c "print($1 / 10)")
  xrandr --output eDP-1 --brightness $R
}

function suspend () {
  i3lock && systemctl suspend
}

function lookup () {
  if [ ! -z "$1" ]; then
    firefox "https://www.lexico.com/en/definition/$1" &
  else
    echo "Please provide an argument."
  fi
}

function today () {
  date +"<%Y-%m-%d %a>"
}


# Customize to your needs...

# OPAM configuration
. /home/ayberkt/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true
