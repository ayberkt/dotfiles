if exists("g:loaded_initvim") || !has('nvim')
  finish
else
  let g:loaded_initvim = 1
end

" Enable true color if supported by version
" Neovim >= 0.1.5
if (has("termguicolors"))
  set termguicolors
endif

" Hi! :)
syntax enable
filetype plugin indent on

" Highlight current line
set cursorline

" Don't nag about unwritten changes
set hidden
" Disable another annoying message. Don't forget to save your work often!
set noswapfile

" Show relative line numbers and show actual line number on current line
set relativenumber number

" Show the line and column number of the cursor position, separated by a comma
set ruler

" Keep 8 screen lines above/below the cursor if possible
set scrolloff=8

" Show current command at the bottom line of screen
set showcmd

" Highlight matching bracket when cursor is on one of them
set showmatch

" Always show tabline
set showtabline=2

" Wrap long line, don't break words
set wrap linebreak

" Ignore case when searching ...
set ignorecase
" ... unless search pattern contains upper case characters
set smartcase

" Do smart autoindenting when starting a new line
set smartindent

""" Keymaps
" Like C and D, yank from cursor to end of line
nnoremap Y y$
" Move over wrapped lines the same as normal lines
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk
" Move to end / beginning of visual (wrapped or normal) line
nnoremap & g&
nnoremap $ g$
nnoremap 0 g0
" Copy/paste and move cursor to end of last operated text or end of putted text
"vnoremap <silent> y y`]
"vnoremap <silent> p p`]
"nnoremap <silent> p p`]
" Auto center on search match
noremap n nzz
noremap N Nzz


""" Abbreviations
" Some common and easy mistakes when typing commands:
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Wq wq
cnoreabbrev WQ wq
cnoreabbrev Tabe tabe

map h <UP>
map k <DOWN>
map j <LEFT>
map l <RIGHT>
imap tn <Esc>

" Use TAB as escape.
nnoremap <Tab> <Esc>
vnoremap <Tab> <Esc>gV
onoremap <Tab> <Esc>
cnoremap <Tab> <C-C><Esc>
inoremap <Tab> <Esc>`^
inoremap <Leader><Tab> <Tab>

set colorcolumn=80
