if diff $HOME/.zshrc zshrc; then
  printf "Checked zshrc.\n";
else
  printf "Problem with zshrc.\n";
  exit 1;
fi;

if diff -r $HOME/.config/nvim nvim; then
  printf "Checked nvim.\n";
  exit 0;
else
  printf "Problem with nvim.\n";
  exit 1;
fi
