set -Ux fish_user_paths /opt/homebrew/bin $fish_user_paths
set -Ux fish_user_paths /usr/local/texlive/2024/bin/universal-darwin $fish_user_paths
set -Ux fish_user_paths /Users/$(whoami)/.local/bin $fish_user_paths
set -Ux fish_user_paths ~/.opam/ocaml.5.0.0/bin $fish_user_paths
set -Ux fish_user_paths /usr/local/smlnj/bin $fish_user_paths
set -Ux fish_user_paths /Users/$(whoami)/.ghcup/bin $fish_user_paths
set -Ux fish_user_paths /Users/$(whoami)/.nimble/bin $fish_user_paths

# TODO Add /usr/local/texlive/2024/texmf-dist/doc/man to MANPATH.
# TODO Add /usr/local/texlive/2024/texmf-dist/doc/info to INFOPATH.

set -U C_INCLUDE_PATH "$(xcrun --show-sdk-path)/usr/include/ffi"

if status is-interactive
    # Commands to run in interactive sessions can go here
end

source ~/.iterm2_shell_integration.fish

## Choose prompt.
fish_config prompt choose arrow

# set -x CHRUBY_ROOT /opt/homebrew

# source /opt/homebrew/share/fish/vendor_functions.d/chruby.fish
# source /opt/homebrew/share/fish/vendor_functions.d/chruby_use.fish
# source /opt/homebrew/share/fish/vendor_functions.d/chruby_reset.fish
# chruby ruby-3.1.2

fish_add_path /opt/homebrew/opt/llvm/bin

export GPG_TTY=$(tty)

# alias vim="nvim"
# alias vi="nvim"

# opam configuration
source /Users/$(whoami)/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true

function cp-submission
    cp -f $argv ~/Teaching/fp-teaching/FunProg2022/Tests/Test1-retake/Test1-retakeMarking/ClassTest1Retake.hs
end

function load_dark_colors
    set -U fish_color_normal normal
    set -U fish_color_command c397d8
    set -U fish_color_quote b9ca4a
    set -U fish_color_redirection 70c0b1
    set -U fish_color_end c397d8
    set -U fish_color_error d54e53
    set -U fish_color_param 7aa6da
    set -U fish_color_comment e7c547
    set -U fish_color_match --background=brblue
    set -U fish_color_selection white --bold --background=brblack
    set -U fish_color_search_match bryellow --background=brblack
    set -U fish_color_history_current --bold
    set -U fish_color_operator 00a6b2
    set -U fish_color_escape 00a6b2
    set -U fish_color_cwd green
    set -U fish_color_cwd_root red
    set -U fish_color_valid_path --underline
    set -U fish_color_autosuggestion 969896
    set -U fish_color_user brgreen
    set -U fish_color_host normal
    set -U fish_color_cancel --reverse
    set -U fish_pager_color_prefix normal --bold --underline
    set -U fish_pager_color_progress brwhite --background=cyan
    set -U fish_pager_color_completion normal
    set -U fish_pager_color_description B3A06D
    set -U fish_pager_color_selected_background --background=brblack
    set -U fish_color_host_remote
    set -U fish_pager_color_background
    set -U fish_pager_color_selected_completion
    set -U fish_color_option
    set -U fish_pager_color_secondary_description
    set -U fish_pager_color_secondary_background
    set -U fish_color_keyword
    set -U fish_pager_color_selected_prefix
    set -U fish_pager_color_secondary_prefix
    set -U fish_pager_color_selected_description
    set -U fish_pager_color_secondary_completion
end

function load_light_colors
  set -U fish_color_normal normal
  set -U fish_color_command 8959a8
  set -U fish_color_quote 718c00
  set -U fish_color_redirection 3e999f
  set -U fish_color_end 8959a8
  set -U fish_color_error c82829
  set -U fish_color_param 4271ae
  set -U fish_color_comment eab700
  set -U fish_color_match --background=brblue
  set -U fish_color_selection white --bold --background=brblack
  set -U fish_color_search_match bryellow --background=brblack
  set -U fish_color_history_current --bold
  set -U fish_color_operator 00a6b2
  set -U fish_color_escape 00a6b2
  set -U fish_color_cwd green
  set -U fish_color_cwd_root red
  set -U fish_color_valid_path --underline
  set -U fish_color_autosuggestion 8e908c
  set -U fish_color_user brgreen
  set -U fish_color_host normal
  set -U fish_color_cancel --reverse
  set -U fish_pager_color_prefix normal --bold --underline
  set -U fish_pager_color_progress brwhite --background=cyan
  set -U fish_pager_color_completion normal
  set -U fish_pager_color_description B3A06D
  set -U fish_pager_color_selected_background --background=brblack
  set -U fish_color_keyword
  set -U fish_color_host_remote
  set -U fish_pager_color_selected_description
  set -U fish_pager_color_secondary_prefix
  set -U fish_pager_color_selected_prefix
  set -U fish_pager_color_secondary_completion
  set -U fish_pager_color_secondary_background
  set -U fish_pager_color_background
  set -U fish_color_option
  set -U fish_pager_color_secondary_description
  set -U fish_pager_color_selected_completion
end

function forest_setup
    echo -n "Running HTTP server..."
    tmux new-session -d -s "axt-forest-server" "cd ~/Code/mathematical-forest && python3 -m http.server 8000 -d output/"; and echo " OK"
    echo -n "Starting watch.sh..."
    tmux new-session -d -s axt-forest-watch "cd ~/Code/mathematical-forest && ./watch.sh"; and echo " OK"
end

function today
    date "+%Y-%m-%d";
end

function generate_agda_html
    echo "Fetching Agda.css..."
    ln -s ~/Code/agda-html-generation/Agda.css Agda.css
    echo "Fetching generate_html.sh"
    ln -s ~/Code/agda-html-generation/generate_html.sh generate_html.sh
    chmod u+x generate_html.sh
    echo "Fetching the Markdown conversion script"
    ln -s ~/Code/agda-html-generation/markdownify.py markdownify.py
end

set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME ; set -gx PATH $HOME/.cabal/bin /Users/$(whoami)/.ghcup/bin $PATH # ghcup-env
