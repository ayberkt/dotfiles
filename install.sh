RED="`tput bold``tput setaf 1`"
GREEN="`tput bold``tput setaf 2`"
YELLOW=`tput setaf 3`
RESET=`tput sgr0`
WHITE="`tput dim``tput setaf 7`"

function make_link_unchecked () {
  if ln -s $1 $2; then
    echo "${GREEN}Succesfully linked ${WHITE}$2 to ${WHITE}$1${RESET}";
    return 0
  else
    echo "Could not link $HOME/.zshrc";
    return 1
  fi
}

function make_link () {
  if [ -e $2 ]; then
    echo "${YELLOW}WARNING: ${WHITE}$2 ${YELLOW}already exists."
    echo "${WHITE}-->${YELLOW} Renaming it to ${WHITE}$2.bak${YELLOW}.${RESET}";
    rm -f $2.bak;
    mv -f $2 $2.bak;
  fi
  make_link_unchecked $1 $2
}

echo "`tput bold`Setting up zsh..."
make_link `pwd`/zshrc $HOME/.zshrc
printf "\n"
echo "`tput bold`Setting up NeoVim..."
make_link `pwd`/nvim $HOME/.config/nvim
